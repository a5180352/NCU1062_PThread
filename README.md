# PThread : First Readers-Writers Problem

* Lecture : NCU 1062 Operating System
* Language : `c++ --std=c++11`
* Environment_1 : Windows 10
    * Compiler : `mingw-w64 (x86_64-7.3.0-posix-seh-rt_v5-rev0)`
* Environment_2 : Ubuntu 16.04 on Windows 10
    * Compiler : `x86_64-linux-gnu (Ubuntu 5.4.0-6ubuntu1~16.04.9)`

## The First Readers-Writers Problem with `pthread`
* Find more details in `1062-OS-HW2.md` or on [Wikipedia](https://en.wikipedia.org/wiki/Readers%E2%80%93writers_problem).
* `m_n.out` is the sample output of `m`-writers-`n`-readers condition.
* Sample output :

```
Input writer m: 1
Input reader n: 3
[writer 1] enters the critical section
[writer 1] updates database: 30
[writer 1] sleeps 10 second..........
[writer 1] exits the critical section
          [reader 1] enters the critical section
          [reader 1] reads database: 30
          [reader 1] sleeps 4 second
          [reader 2] enters the critical section
          [reader 2] reads database: 30
......
          [reader 1] enters the critical section
          [reader 1] reads database: 30
          [reader 1] sleeps 4 second
          [reader 2] exits the critical section
          [reader 3] exits the critical section
          [reader 1] exits the critical section
[writer 1] enters the critical section
[writer 1] updates database: 5
[writer 1] sleeps 5 second.....
[writer 1] exits the critical section
Total updates: 2
Total readings: 112
```