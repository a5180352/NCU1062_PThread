#include <iostream>
#include <windows.h>
#include <pthread.h>
#include <random>
#include <ctime>
using namespace std;

int database = 0;
int updates = 0;
int readings = 0;
int reader_count = 0;

pthread_mutex_t mutex_db = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_rd = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_io = PTHREAD_MUTEX_INITIALIZER;

bool is_finished = false;

default_random_engine generator = default_random_engine(time(NULL));
uniform_int_distribution<int> ram_1_50(1, 50);
uniform_int_distribution<int> ram_1_2(1, 2);

void* writer(void* arg) {
    int w_id = *((int*)arg);
    while (!is_finished) {
        // Enter
        pthread_mutex_lock(&mutex_db);
        cout << "[writer " << w_id << "] enters the critical section" << endl;
        // Write database
        database = ram_1_50(generator);
        cout << "[writer " << w_id << "] updates database: " << database << endl;
        updates++;
        // Sleep 5 or 10 seconds
        int sleep_time = ram_1_2(generator) * 5;
        cout << "[writer " << w_id << "] sleeps " << sleep_time << " second";
        cout.flush();
        for (int i = 0; i < sleep_time; i++) {
            Sleep(1000);
            cout << ".";
            cout.flush();
        }
        cout << endl;
        // Leave
        cout << "[writer " << w_id << "] exits the critical section" << endl;
        pthread_mutex_unlock(&mutex_db);
        Sleep(1);
    }
    pthread_exit(NULL);
}

void* reader(void* arg) {
    int r_id = *((int*)arg);
    while (!is_finished) {
        // Enter
        pthread_mutex_lock(&mutex_rd);
        reader_count++;
        if (reader_count == 1) {
            pthread_mutex_lock(&mutex_db); // Lock database if you are the first reader
        }
        pthread_mutex_lock(&mutex_io);
        cout << "          [reader " << r_id << "] enters the critical section" << endl;
        pthread_mutex_unlock(&mutex_io);
        pthread_mutex_unlock(&mutex_rd);
        // Read database
        pthread_mutex_lock(&mutex_io);
        cout << "          [reader " << r_id << "] reads database: " << database << endl;
        pthread_mutex_unlock(&mutex_io);
        // Sleep 2 or 4 seconds
        int sleep_time = ram_1_2(generator) * 2;
        pthread_mutex_lock(&mutex_io);
        cout << "          [reader " << r_id << "] sleeps " << sleep_time << " second" << endl;
        pthread_mutex_unlock(&mutex_io);
        Sleep(sleep_time * 1000);
        // Leave
        pthread_mutex_lock(&mutex_rd);
        readings++;
        reader_count--;
        pthread_mutex_lock(&mutex_io);
        cout << "          [reader " << r_id << "] exits the critical section" << endl;
        pthread_mutex_unlock(&mutex_io);
        if (reader_count == 0) {
            pthread_mutex_unlock(&mutex_db); // Unlock if you are the last reader
        }
        pthread_mutex_unlock(&mutex_rd);
        Sleep(1);
    }
    pthread_exit(NULL);
}

int main() {
    unsigned int m, n;
    cout << "Input writer m: ";
    cin >> m;
    cout << "Input reader n: ";
    cin >> n;

    pthread_t w[m], r[n];
    for (unsigned int i = 0; i < m; i++) {
        int* num = new int();
        *num = i + 1;
        pthread_create(&w[i], NULL, writer, (void*)num);
    }
    for (unsigned int i = 0; i < n; i++) {
        int* num = new int();
        *num = i + 1;
        pthread_create(&r[i], NULL, reader, (void*)num);
    }

    Sleep(2 * 60 * 1000);
    is_finished = true;

    for (unsigned int i = 0; i < m; i++) {
        pthread_join(w[i], NULL);
    }
    for (unsigned int i = 0; i < n; i++) {
        pthread_join(r[i], NULL);
    }

    cout << endl;
    cout << "Total updates : " << updates << endl;
    cout << "Total readings: " << readings << endl;
}